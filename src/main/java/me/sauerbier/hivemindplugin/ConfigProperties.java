/*
 * Copyright 2018 by Jan Hof. All rights reserved
 */

package me.sauerbier.hivemindplugin;

import me.sauerbier.hivemind.protocol.ConnectionProperties;

import java.util.List;
import java.util.Locale;

public class ConfigProperties implements ConnectionProperties{

    private String nodeName;
    private String protocol;
    private List<String> hosts;
    private int timeout;
    private int brokerIndex;
    private int threads;
    private String master;
    private String password;
    private String logLevel;

    public ConfigProperties() {
    }

    public String getProtocol() {
        return this.protocol;
    }

    @Override
    public List<String> getHosts(){
        return this.hosts;
    }

    @Override
    public int getTimeout(){
        return this.timeout;
    }

    @Override
    public int getBrokerIndex(){
        return this.brokerIndex;
    }

    @Override
    public String getMaster(){
        return this.master;
    }

    @Override
    public String getPassword(){
        return this.password;
    }

    public String getLogLevel(){
        return logLevel.toUpperCase(Locale.ROOT);
    }

    public int getThreads(){
        return threads;
    }

    public void setThreads(int threads){
        this.threads = threads;
    }

    @Override
    public String getNodeName(){
        return this.nodeName;
    }

    public void setNodeName(String nodeName){
        this.nodeName = nodeName;
    }
}
