package me.sauerbier.hivemindplugin;

import me.sauerbier.hivemind.protocol.InvocationAgent;
import me.sauerbier.hivemind.utils.Params;
import org.bukkit.Bukkit;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.Future;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2021 by Jan Hof
 * All rights reserved.
 **/
public class BukkitInvocationAgent implements InvocationAgent{

    @Override
    public <T, R> Future<R> invokeSync(Method method, Params params, T target){
        if(params.isEmpty()){
            return Bukkit.getScheduler().callSyncMethod(HiveMindPlugin.instance, () -> (R) method.invoke(target));
        } else{
            return Bukkit.getScheduler().callSyncMethod(HiveMindPlugin.instance, () -> (R) method.invoke(target, params.getObjects()));
        }
    }

    @Override
    public <T, R> R invokeAsync(Method method, Params params, T target) throws InvocationTargetException, IllegalAccessException{
        if(params.isEmpty()){
            return (R) method.invoke(target);
        }else{
            return (R) method.invoke(target, params.getObjects());
        }
    }
}
