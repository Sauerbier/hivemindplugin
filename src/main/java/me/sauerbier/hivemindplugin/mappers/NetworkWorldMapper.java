/*
 * Copyright 2018 by Jan Hof. All rights reserved
 */

package me.sauerbier.hivemindplugin.mappers;

import me.sauerbier.hivemind.HiveMind;
import me.sauerbier.hivemind.protocol.NetworkObjectMapper;
import org.bukkit.Bukkit;

import java.util.UUID;

public class NetworkWorldMapper extends NetworkObjectMapper{
    public NetworkWorldMapper(final HiveMind hiveMind){
        super(hiveMind);
    }

    @Override
    public Object mapToRealObject(final Object target){
        try{
            return Bukkit.getWorld(UUID.fromString(target.toString()));
        }catch(IllegalArgumentException e){
            return null;
        }
    }
}
