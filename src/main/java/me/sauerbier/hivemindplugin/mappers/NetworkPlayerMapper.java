/*
 * Copyright 2018 by Jan Hof. All rights reserved
 */

package me.sauerbier.hivemindplugin.mappers;

import me.sauerbier.hivemind.HiveMind;
import me.sauerbier.hivemind.protocol.NetworkObjectMapper;
import org.bukkit.Bukkit;

public class NetworkPlayerMapper extends NetworkObjectMapper{
    public NetworkPlayerMapper(final HiveMind hiveMind){
        super(hiveMind);
    }


    @Override
    public Object mapToRealObject(final Object target){
        return Bukkit.getPlayer(target.toString());
    }
}
