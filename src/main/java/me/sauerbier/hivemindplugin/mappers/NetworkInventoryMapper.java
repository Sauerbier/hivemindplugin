package me.sauerbier.hivemindplugin.mappers;

import me.sauerbier.hivemind.HiveMind;
import me.sauerbier.hivemind.protocol.NetworkObjectMapper;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Optional;
import java.util.UUID;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2022 by Jan Hof
 * All rights reserved.
 **/
public class NetworkInventoryMapper extends NetworkObjectMapper{
    public NetworkInventoryMapper(HiveMind hiveMind){
        super(hiveMind);
    }

    @Override
    public Object mapToRealObject(Object target){
        return Optional.ofNullable(target)
                .map(Object::toString)
                .map(UUID::fromString)
                .map(Bukkit::getPlayer)
                .map(Player::getInventory)
                .orElse(null);
    }
}
