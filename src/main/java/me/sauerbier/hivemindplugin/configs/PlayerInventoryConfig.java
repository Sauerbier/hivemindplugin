package me.sauerbier.hivemindplugin.configs;

import org.bukkit.inventory.PlayerInventory;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2022 by Jan Hof
 * All rights reserved.
 **/
public interface PlayerInventoryConfig extends PlayerInventory{
}
