package me.sauerbier.hivemindplugin.configs;

import me.sauerbier.hivemind.Sync;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.jetbrains.annotations.NotNull;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2021 by Jan Hof
 * All rights reserved.
 **/
@Sync
public interface WorldClassConfig extends World{

    @Override
    boolean unloadChunkRequest(int i, int i1);

    @Override
    boolean unloadChunk(@NotNull Chunk chunk);

    @Override
    boolean unloadChunk(int i, int i1);

    @Override
    boolean unloadChunk(int i, int i1, boolean b);

    @Override
    boolean loadChunk(int i, int i1, boolean b);

    @Override
    void loadChunk(@NotNull Chunk chunk);

    @Override
    void loadChunk(int i, int i1);

}
