package me.sauerbier.hivemindplugin.configs;

import me.sauerbier.hivemind.IgnoreReturn;
import me.sauerbier.hivemind.Sync;
import net.kyori.adventure.text.Component;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2021 by Jan Hof
 * All rights reserved.
 **/

@Sync
public interface PlayerClassConfig extends Player{

    void kick(@Nullable Component component);

    void kick(@Nullable Component component, PlayerKickEvent.@NotNull Cause cause);

    void kickPlayer(@Nullable String s);

    void chat(@NotNull String s);

    @IgnoreReturn
    boolean teleport(@NotNull Entity entity);

    @IgnoreReturn
    boolean teleport(@NotNull Location location);

    @IgnoreReturn
    boolean teleport(@NotNull Entity entity, PlayerTeleportEvent.@NotNull TeleportCause teleportCause);

    @IgnoreReturn
    boolean teleport(@NotNull Location location, PlayerTeleportEvent.@NotNull TeleportCause teleportCause);

}
