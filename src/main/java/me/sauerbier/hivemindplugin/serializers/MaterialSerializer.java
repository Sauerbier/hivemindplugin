package me.sauerbier.hivemindplugin.serializers;

import com.google.gson.*;
import me.sauerbier.hivemind.HiveMind;
import me.sauerbier.hivemind.serialisation.TypeAdapter;
import org.bukkit.Material;

import java.lang.reflect.Type;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2022 by Jan Hof
 * All rights reserved.
 **/
public class MaterialSerializer extends TypeAdapter<Material>{
    public MaterialSerializer(HiveMind hiveMind){
        super(hiveMind);
    }

    @Override
    public Material deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException{
        return Material.getMaterial(jsonElement.getAsString());
    }

    @Override
    public JsonElement serialize(Material material, Type type, JsonSerializationContext jsonSerializationContext){
        return new JsonPrimitive(material.name());
    }
}
