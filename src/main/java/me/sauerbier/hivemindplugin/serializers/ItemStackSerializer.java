package me.sauerbier.hivemindplugin.serializers;

import com.google.common.reflect.TypeToken;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import me.sauerbier.hivemind.HiveMind;
import me.sauerbier.hivemind.serialisation.TypeAdapter;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2022 by Jan Hof
 * All rights reserved.
 **/
public class ItemStackSerializer extends TypeAdapter<ItemStack>{


    public ItemStackSerializer(HiveMind hiveMind){
        super(hiveMind);
    }

    @Override
    public ItemStack deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException{
        return ItemStack.deserialize(jsonDeserializationContext.deserialize(jsonElement, new TypeToken<Map<String, Object>>(){}.getType()));
    }

    @Override
    public JsonElement serialize(ItemStack itemStack, Type type, JsonSerializationContext jsonSerializationContext){
        return jsonSerializationContext.serialize(itemStack.serialize());
    }
}
