/*
 * Copyright 2018 by Jan Hof. All rights reserved
 */

package me.sauerbier.hivemindplugin.serializers;

import com.google.gson.*;
import me.sauerbier.hivemind.HiveMind;
import me.sauerbier.hivemind.serialisation.RecordsTypeAdapterFactory;
import me.sauerbier.hivemind.serialisation.TypeAdapter;
import org.bukkit.World;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class ReferenceSerializer extends TypeAdapter<Reference<?>>{
    private final Gson gson;

    public ReferenceSerializer(final HiveMind hiveMind) {
        super(hiveMind);
        this.gson = new GsonBuilder()
                .registerTypeAdapterFactory(new RecordsTypeAdapterFactory())
                .registerTypeHierarchyAdapter(World.class, new WorldSerializer(super.getHiveMind()))
                .create();
    }

    @Override
    public Reference<?> deserialize(final JsonElement jsonElement, final Type type, final JsonDeserializationContext jsonDeserializationContext) throws JsonParseException{
        return new WeakReference<>(this.gson.fromJson(jsonElement, type instanceof ParameterizedType ? ((ParameterizedType) type).getActualTypeArguments()[0] : type));
    }

    @Override
    public JsonElement serialize(final Reference<?> ref, final Type type, final JsonSerializationContext jsonSerializationContext){
        return this.gson.toJsonTree(ref.get());
    }
}
