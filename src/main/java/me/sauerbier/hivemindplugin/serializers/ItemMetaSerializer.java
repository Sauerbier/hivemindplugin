package me.sauerbier.hivemindplugin.serializers;

import com.google.gson.*;
import me.sauerbier.hivemind.HiveMind;
import me.sauerbier.hivemind.serialisation.TypeAdapter;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Type;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2022 by Jan Hof
 * All rights reserved.
 **/
public class ItemMetaSerializer extends TypeAdapter<ItemMeta>{

    public ItemMetaSerializer(HiveMind hiveMind){
        super(hiveMind);
    }

    @Override
    public ItemMeta deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException{
        return null;
    }

    @Override
    public JsonElement serialize(ItemMeta itemMeta, Type type, JsonSerializationContext jsonSerializationContext){
        return new JsonObject();
    }
}
