package me.sauerbier.hivemindplugin.serializers;

import com.google.gson.*;
import me.sauerbier.hivemind.HiveMind;
import me.sauerbier.hivemind.serialisation.TypeAdapter;
import me.sauerbier.hivemindplugin.HiveMindPlugin;
import org.bukkit.inventory.PlayerInventory;

import java.lang.reflect.Type;
import java.util.UUID;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2022 by Jan Hof
 * All rights reserved.
 **/
public class InventorySerializer extends TypeAdapter<PlayerInventory>{

    public InventorySerializer(HiveMind hiveMind){
        super(hiveMind);
    }

    @Override
    public PlayerInventory deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException{
        return HiveMindPlugin.getInventory(UUID.fromString(jsonElement.getAsString()));
    }

    @Override
    public JsonElement serialize(PlayerInventory inventory, Type type, JsonSerializationContext jsonSerializationContext){
        return new JsonPrimitive(inventory.getHolder().getUniqueId().toString());
    }
}
