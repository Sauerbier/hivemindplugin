/*
 * Copyright 2018 by Jan Hof. All rights reserved
 */

package me.sauerbier.hivemindplugin.serializers;

import com.google.gson.*;
import me.sauerbier.hivemind.HiveMind;
import me.sauerbier.hivemind.serialisation.TypeAdapter;
import me.sauerbier.hivemindplugin.HiveMindPlugin;
import org.bukkit.World;

import java.lang.reflect.Type;
import java.util.UUID;

public class WorldSerializer extends TypeAdapter<World>{

    public WorldSerializer(final HiveMind hiveMind) {
      super(hiveMind);
    }

    @Override
    public World deserialize(final JsonElement jsonElement, final Type type, final JsonDeserializationContext jsonDeserializationContext) throws JsonParseException{
        return HiveMindPlugin.getWorld(UUID.fromString(jsonElement.getAsString()));
    }

    @Override
    public JsonElement serialize(final World world, final Type type, final JsonSerializationContext jsonSerializationContext){
        return new JsonPrimitive( world.getUID().toString());
    }
}
