package me.sauerbier.hivemindplugin;

import de.sauerbier.mc.mcutils.concurrent.PooledTimerFactory;
import de.sauerbier.mc.mcutils.config.Config;
import de.sauerbier.mc.mcutils.config.ConfigReader;
import me.sauerbier.hivemind.HiveMind;
import me.sauerbier.hivemind.HiveMindBuilder;
import me.sauerbier.hivemindplugin.commands.IPBCommand;
import me.sauerbier.hivemindplugin.configs.PlayerClassConfig;
import me.sauerbier.hivemindplugin.configs.PlayerInventoryConfig;
import me.sauerbier.hivemindplugin.configs.WorldClassConfig;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;
import java.util.logging.Level;

/**
 * @Author Sauerbier | Jan
 * @Copyright 2021 by Jan Hof
 * All rights reserved.
 **/
public class HiveMindPlugin extends JavaPlugin{

    public static HiveMindPlugin instance;
    public static PooledTimerFactory timerFactory;



    // /ipb -x xLaura25x test message

    @Config("hivemind")
    private ConfigProperties config;

    private HiveMind hiveMind;


    @Override
    public void onEnable() {
        instance = this;
        timerFactory = new PooledTimerFactory("prog");

        this.getConfig().options().copyDefaults(true);
        this.saveDefaultConfig();
        ConfigReader.read(this);

        this.getCommand("ipb").setExecutor(new IPBCommand());
        this.getCommand("log").setExecutor((CommandSender commandSender, Command command, String s, String[] args) -> {
            this.getLogger().setLevel(Level.parse(args[0]));
            return true;
        });

        this.getLogger().setLevel(Level.parse(config.getLogLevel()));

        try{
            this.hiveMind = new HiveMindBuilder(config)
                    .protocol()
                    .fromName(this.config.getProtocol())
                    .mapper("me.sauerbier.hivemindplugin.mappers")
                    .registerType("me.sauerbier.hivemindplugin.serializers")
                    .invocationAgent(new BukkitInvocationAgent())
                    .back()
                    .build();
        } catch(InvocationTargetException | NoSuchMethodException | InstantiationException | IllegalAccessException | IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }


    /**
     * @param name Player Account name
     * @return Interplanetary player object if the player is online on a different node
     * otherwise a legacy CraftPlayer object is returned.
     * <b>Note!</b> If the player is not online ether here nor on a different node,
     * an interplanetary player is still returned, but all method calls may timeout or
     * result in null
     */
    public static Player getPlayer(final String name){
        final Player player = instance.getServer().getPlayer(name);
        if(player != null){
            return player;
        }
        return instance.hiveMind.interplanetarize(Player.class, name).configureMethods(PlayerClassConfig.class).get();
    }

    public static World getWorld(final UUID uuid){
        final World world = Bukkit.getWorld(uuid);
        if(world != null){
            return world;
        }

        return instance.hiveMind.interplanetarize(World.class, uuid.toString()).configureMethods(WorldClassConfig.class).get();
    }

    public static PlayerInventory getInventory(final UUID uuid){
        final Player player = instance.getServer().getPlayer(uuid);
        if(player != null){
            return player.getInventory();
        }

        return instance.hiveMind.interplanetarize(PlayerInventory.class, uuid.toString()).configureMethods(PlayerInventoryConfig.class).get();
    }

    @Override
    public void onDisable() {
        this.hiveMind.close();
        timerFactory.close();
    }

    public HiveMind getHiveMind() {
        return this.hiveMind;
    }
}
